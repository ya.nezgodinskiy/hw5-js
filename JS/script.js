// 1. Опишіть своїми словами, що таке метод об'єкту
// Метод об'єкту це вбудована в об'єкт функція.

// 2. Який тип даних може мати значення властивості об'єкта?
// Значення властивості об'кта може мати будь який тип данних з існуючих.

// 3. Об'єкт це посилальний тип даних. Що означає це поняття?
// Я думаю це означає те, що при копіюванні об'кту зберігається не данні в них, а посилання на данні і із-за цього і назва.

// Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.

function createNewUser() {
  let newUser = {};
  let firstName = prompt("Ваше ім'я?");
  let secondName = prompt("Ваше прізвище?");
  newUser.firstName = firstName;
  newUser.secondName = secondName;
  newUser.getLogin = function() {
    let firstInitial = firstName[0].toLowerCase();
    firstInitial
    let login = firstInitial + secondName.toLowerCase();
    return login;
  } 
return newUser;
}

let user = createNewUser();
console.log(user.getLogin());